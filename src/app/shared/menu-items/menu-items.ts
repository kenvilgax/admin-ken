import { Injectable } from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  name: string;
  type?: string;
}

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

const MENUITEMS = [
  {
    state: '/',
    name: 'HOME',
    type: 'link',
    icon: 'basic-accelerator'
  },
  {
    state: 'forms',
    name: 'New Users',
    type: 'sub',
    icon: 'basic-sheet-pen',
    children: [
      {
        state: 'basic',
        name: 'Add new User'
      }]},



  {
    state: 'datatable',
    name: 'Registerd Users',
    type: 'sub',
    icon: 'basic-webpage-img-txt',
    children: [

      {
        state: 'filter',
        name: 'View'
      },
    ]
  },
  {
    state: 'charts',
    name: 'CHARTS',
    type: 'sub',
    icon: 'ecommerce-graph1',
    badge: [

    ],
    children: [
      {
        state: 'bar',
        name: 'BAR'
      },

    ]
  },

  {
    state: 'pages',
    name: 'Incidenc',
    type: 'sub',
    icon: 'basic-spread-text-bookmark',
    children: [


      {
        state: 'blank',
        name: 'Review Incident'
      },
      {
        state: 'blank',
        name: 'Manage Incident'
      },
    ]
  },

  {
    state: 'authentication',
    name: 'Settings',
    type: 'sub',
    icon: 'basic-lock-open',
    children: [

      {
        state: 'forgot',
        name: 'Forgot password'
      },
      {
        state: 'lockscreen',
        name: 'LOCKSCREEN'
      },
    ]
  },


];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }



}
